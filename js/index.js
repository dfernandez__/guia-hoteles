$(() => {
    $('[data-toggle="tooltip"]').tooltip();
    $('.carousel').carousel({
        interval: 2000
    });

    $('#contactos').on('show.bs.modal', e => {
        console.log("Mostrando...");
    });
    $('#contactos').on('shown.bs.modal', e => {
        console.log("Mostro");
        $('#contactoBtn').removeClass('btn-outline-success');
        $('#contactoBtn').addClass('btn-default').attr('disabled', true)
    });
    $('#contactos').on('hide.bs.modal', e => {
        console.log("Ocultando...");
        $('#contactoBtn').addClass('btn-outline-success');
        $('#contactoBtn').removeClass('btn-default').attr('disabled', false)
    });

    $('#contactos').on('hidden.bs.modal', e => {
        console.log("Se oculto");
    });
})